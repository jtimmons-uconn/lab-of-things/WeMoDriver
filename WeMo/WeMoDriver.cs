﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HomeOS.Hub.Common;
using HomeOS.Hub.Platform.Views;
using System.Threading;

namespace HomeOS.Hub.Drivers.WeMo
{

    /// <summary>
    /// A dummy driver module that 
    /// 1. opens and register a dummy ports
    /// 2. sends periodic notifications  (in Work())
    /// 3. sends back responses to received echo requests (in OnOperationInvoke())
    /// </summary>

    [System.AddIn.AddIn("HomeOS.Hub.Drivers.WeMo")]
    public class DriverWeMo : ModuleBase
    {
        SafeThread _backgroundWorker = null;
        Port _port;

        private WebFileServer imageServer;

        public override void Start()
        {
            logger.Log("Started: {0}", ToString());

            string deviceName = moduleInfo.Args()[0];

            // Instantiate the virtual port
            VPortInfo portInfo = GetPortInfoFromPlatform(deviceName);
            _port = InitPort(portInfo);

            // Create a list of roles that the port will do and export
            List<VRole> listRole = new List<VRole>() { RoleSwitchBinary.Instance };
            BindRoles(_port, listRole);

            // Register the port with the system
            RegisterPortWithPlatform(_port);

            _backgroundWorker = new SafeThread(Work, "WeMoDriver work thread", logger);
            _backgroundWorker.Start();

            imageServer = new WebFileServer(moduleInfo.BinaryDir(), moduleInfo.BaseURL(), logger);
        }

        public override void Stop()
        {
            logger.Log("Stop() at {0}", ToString());
            if (_backgroundWorker != null)
                _backgroundWorker.Abort();
            imageServer.Dispose();
        }


        /// <summary>
        /// Sit in a loop and send notifications 
        /// </summary>
        public void Work()
        {
            // Nothing to do here ... could check the existence of the device?
        }

        /// <summary>
        /// The demultiplexing routing for incoming operations
        /// </summary>
        /// <param name="message"></param>
        public override IList<VParamType> OnInvoke(string roleName, String opName, IList<VParamType> args)
        {

            if (!roleName.Equals(RoleSwitchBinary.RoleName))
            {
                logger.Log("Invalid role {0} in OnInvoke", roleName);
                return null;
            }

            switch (opName.ToLower())
            {
                case RoleSwitchBinary.OpGetName:
                    logger.Log("{0} Got GetRequest", this.ToString());

                    return new List<VParamType>() { new ParamType(false) };
                case RoleSwitchBinary.OpSetName:
                    logger.Log("{0} Got SetRequest", this.ToString());
                    return new List<VParamType>() {};
                default:
                    logger.Log("Invalid operation: {0}", opName);
                    return null;
            }
        }

        /// <summary>
        ///  Called when a new port is registered with the platform
        ///        the dummy driver does not care about other ports in the system
        /// </summary>
        public override void PortRegistered(VPort port) { }

        /// <summary>
        ///  Called when a new port is deregistered with the platform
        ///        the dummy driver does not care about other ports in the system
        /// </summary>
        public override void PortDeregistered(VPort port) { }
    }
}